﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace AEV7
{
    class Empleados
    {
        private string nif;
        private string nombre;
        private string apellido;
        private bool admin;
        private string claveAdmin;

        public Empleados(string nif, string nombre, string apellido, bool admin, string claveAdmin)
        {
            this.nif = nif;
            this.nombre = nombre;
            this.apellido = apellido;
            this.admin = admin;
            this.claveAdmin = claveAdmin;
        }
        public Empleados()
        {

        }
        /// <summary>
        /// Metodo que recibe un string con el dni y realiza el fichaje de entrada
        /// </summary>
        /// <param name="dni"></param>

        public void EntradaFichaje(string dni)
        {

            BasesDatos.AbrirConexion();

            Fichaje fichar1 = new Fichaje(dni, DateTime.Now.Date,DateTime.Now,DateTime.MinValue, 1, 0, 0);

            

            string consulta = string.Format("INSERT INTO `fichajes` (`nif`, `fecha`, `horaentrada`, `horasalida`, `entrada`, `salida`, `completo`) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}');",fichar1.Dni, fichar1.Fecha.ToString("yyyy/MM/dd"),fichar1.HoraEntrada.ToString("yyyy/MM/dd HH:mm:ss"), fichar1.HoraSalida,fichar1.Entrada,fichar1.Salida,fichar1.Completo);

            MySqlConnection conexion = BasesDatos.Conexion;

            MySqlCommand comando = new MySqlCommand(consulta, conexion);

            int retorno = comando.ExecuteNonQuery();

            if (retorno > 0)
            {
                MessageBox.Show("Se a Fichado correctamente");
            }
            else
            {
                MessageBox.Show("El fichaje no a sido posible contacte con el administrador");
            }

            






        }

        /// <summary>
        /// Metodo para realizar el fichaje de salida
        /// </summary>
        public void SalidaFichaje()
        {

        }







        public string Comprobardni(string nif)
        {
            string dni = nif.Substring(0, 8);
            int valorDni;
            int indice;
            const string tabla = "TRWAGMYFPDXBNJZSQVHLCKET";


            //for (int i = 1; i <= 8; i++)
            //{
            //    dni += Convert.ToString(dni.Next(10));
            //}

            valorDni = Convert.ToInt32(dni);
            indice = valorDni % 23;
            dni = dni + tabla[indice]; // concateno la letrcorrespondiente a dni

            return dni;

        }
        /// <summary>
        /// Metodo que va a comprobar si el dni introducido se corresponde a un empleado 
        /// </summary>
        /// <returns></returns>
        public bool ComprobarAlta(string dni){

            //realmente le tendremos que pasar txtDNI.Text
            string consulta = string.Format("SELECT `nif` FROM `empleados` WHERE `nif` LIKE '{0}'", dni);

            MySqlConnection conexion = BasesDatos.Conexion;
            MySqlCommand comando = new MySqlCommand(consulta, conexion);

            BasesDatos.AbrirConexion();

            MySqlDataReader resultado = comando.ExecuteReader();
            bool esta = resultado.Read();

            //string numero = null;
            //if (resultado.HasRows)   // En caso que se hayan registros en el objeto reader
            //{
            //    // Recorremos el resultado y cargamos la lista de usuarios.
            //    while (resultado.Read())
            //    {
            //        numero = resultado.GetString(0);
            //    }
            //}

            //MessageBox.Show(numero);

            if (esta == true)
            {
                MessageBox.Show("El empleado está dado de alta.");
            }
            else
            {
                MessageBox.Show("NO EXISTE en la base de datos.");

            }
            BasesDatos.CerrarConexion();

            return esta;


        }


        /// <summary>
        /// Metodo que comprueba si el usuario ya a realizado el fichaje de Entrada
        /// </summary>

        public int ComprobarEntrada(string dni)
        {
            //consulta que de un bool y me diga si ha fichado al entrar

            // Añadir a la consulta otra condicion que compruebe si tiene un fichaje finalizado ********************************************

            //primero comprobar si tiene un fichaje sin finalizar

            string consulta = string.Format("SELECT * FROM `fichajes` WHERE `nif` LIKE '{0}' AND `entrada` = 1 AND `completo` = 0", dni);

            MySqlConnection conexion = BasesDatos.Conexion;
            MySqlCommand comando = new MySqlCommand(consulta, conexion);

            BasesDatos.AbrirConexion();

            MySqlDataReader resultado = comando.ExecuteReader();

            int estaDentro = 0;

            

            if (resultado.HasRows)   // En caso que se hayan registros en el objeto reader
            {
                // Recorremos el resultado 
                while (resultado.Read())
                {
                    estaDentro = resultado.GetInt32(4);
                }

                


            }


            BasesDatos.CerrarConexion();



            return estaDentro;


        }

        ///// <summary>
        ///// Metodo que comprueba si el usuario ya a realizado la Salida
        ///// </summary>
        ///// <returns></returns>


        //public bool ComprobarSalida()
        //{
        //    //consulta que de un bool y me diga si ha fichado al salir
        //}

        public bool ComprobarAdmin(string dni)
        {

            //realmente le tendremos que pasar txtDNI.Text
            string consulta = string.Format("SELECT `nif` FROM `empleados` WHERE `nif` LIKE '{0}' AND `admin` = 1 ", dni);

            MySqlConnection conexion = BasesDatos.Conexion;
            MySqlCommand comando = new MySqlCommand(consulta, conexion);

            BasesDatos.AbrirConexion();

            MySqlDataReader resultado = comando.ExecuteReader();
            bool esta = resultado.Read();

            //string numero = null;
            //if (resultado.HasRows)   // En caso que se hayan registros en el objeto reader
            //{
            //    // Recorremos el resultado y cargamos la lista de usuarios.
            //    while (resultado.Read())
            //    {
            //        numero = resultado.GetString(0);
            //    }
            //}

            //MessageBox.Show(numero);

            if (esta == true)
            {
                MessageBox.Show("El empleado es admin");
            }
            else
            {
                MessageBox.Show("El empleado No es administrador");

            }
            BasesDatos.CerrarConexion();

            return esta;


        }
        public string ComprobarCont(string dni)
        {

            //realmente le tendremos que pasar txtDNI.Text
            string consulta = string.Format("SELECT * FROM `empleados` WHERE `nif` LIKE '{0}' AND `admin` = 1 ", dni);

            MySqlConnection conexion = BasesDatos.Conexion;
            MySqlCommand comando = new MySqlCommand(consulta, conexion);

            BasesDatos.AbrirConexion();

            string password = "hola";

            MySqlDataReader resultado = comando.ExecuteReader();

            if (resultado.Read())
            {
                password = resultado.GetString(4);
            }

            BasesDatos.CerrarConexion();
            

            

            

            
            //if (resultado.HasRows)   // En caso que se hayan registros en el objeto reader
            //{
            //    // Recorremos el resultado y cargamos la lista de usuarios.
            //    while (resultado.Read())
            //    {
            //        password = resultado.ro;
            //    }
            //}

            

            

            return password;


        }




    }
}
