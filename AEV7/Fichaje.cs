﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEV7
{
    class Fichaje
    {

        // Atributos 
        string nif;
        DateTime fecha = new DateTime();
        DateTime horaEntrada = new DateTime();
        DateTime horaSalida = new DateTime();
        int entrada;
        int salida;
        int completo;

        // Constructor de la clase fichaje
        public Fichaje(string nif,DateTime fecha,DateTime horaEntrada, DateTime horaSalida, int entrada,int salida,int completo)
        {

            this.nif = nif;
            this.fecha = DateTime.Today;
            this.horaEntrada = horaEntrada;
            this.horaSalida = horaSalida;
            this.entrada = entrada;
            this.salida = salida;
            this.completo = completo;

        }
        // Acceso a atributos
        public string Dni { get=> this.nif; }
        public DateTime Fecha { get => this.fecha; }
        public DateTime HoraEntrada { get => this.horaEntrada; }
        public DateTime HoraSalida { get => this.horaSalida; }
        public int Entrada { get => this.entrada; }
        public int Salida { get => this.salida; }

        public int Completo { get => this.completo; }
        
    }
}
