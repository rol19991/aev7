﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AEV7
{
    public partial class FormFichar : Form
    {
        Empleados emp1 = new Empleados();

        public FormFichar()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Boton que raliza las comprobaciones y ejecuta la entra en caso de que las supere.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        


        private void btnEntrada_Click(object sender, EventArgs e)
        {
            Empleados emp1 = new Empleados();
            string nif = txtDNI.Text;
            nif.ToUpper();
            
            if(emp1.Comprobardni(nif) == txtDNI.Text.ToUpper())
            {
                //hago la entrada y abro el datagridview
                MessageBox.Show("La letra coincide");
                //MessageBox.Show(nif);
                //MessageBox.Show("Se va a producir la entrada.");

                if (emp1.ComprobarAlta(nif)){

                    
                    if (emp1.ComprobarEntrada(nif) == 1)
                    {
                        MessageBox.Show("Este usuario ya ha fichado la entrada.");

                    }
                    else
                    {
                        MessageBox.Show("Puede fichar.");
                        emp1.EntradaFichaje(nif);
                    }

                }



            }
            else
            {
                //No hago la entrada y lo comunico falta mostrar el string asi no lo muestra ***
                MessageBox.Show("El dni NO es valido. Debería ser {0}", emp1.Comprobardni(nif));
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            //txtDNI.Text = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            
            
            



        }



        /// <summary>
        /// Boton que hace las comprobaciones y ejecutal la salida del usuario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSalida_Click(object sender, EventArgs e)
        {
            Empleados emp1 = new Empleados();
            string nif = txtDNI.Text;

            if (emp1.Comprobardni(nif) == txtDNI.Text)
            {
                MessageBox.Show("El dni existe.");

                 //consulta bbdd para saber si existe el empleado (Este if sobraria ya que si a fichado la entrada si que esta dado de alta)
                if (emp1.ComprobarAlta(nif))
                {
                    //compruebo ha fichado al entrar
                    if (emp1.ComprobarEntrada(nif) == 1)
                    {
                        MessageBox.Show("Este usuario ya ha fichado.");
                    }
                    else
                    {
                        MessageBox.Show("Puede fichar.");
                        //hago la entrada con un INSERT
                    }

                }
            }
            else
            {
                //no hago la salida y lo comunico
                MessageBox.Show("El dni {0} no se corresponde con el de ningun empleado.", emp1.Comprobardni(nif));
            }




            //if(emp1.ComprobarEntrada() == true)
            //{
            //    MessageBox.Show("El usuario habia fichado al entrar");
            //    emp1.SalidaFichaje();
            //}
            //else
            //{
            //    MessageBox.Show("El empleado no habia fichado al entrar.");
            //}
            

        }

        private void btnPresencia_Click(object sender, EventArgs e)
        {
            //consulta a la base datos para que me muestre todos los empleados que han fichado entrada 
            //nombre
            //apellidos
            //hora de entrada
        }

        private void btnPermanencia_Click(object sender, EventArgs e)
        {
            Empleados emp1 = new Empleados();
            string nif = "21010322Y";

            //comprobar su dni
            if (emp1.Comprobardni(nif) == txtDNI.Text)
            {
                MessageBox.Show("El dni existe.");

                //consulta bbdd para saber si existe el empleado
                if (emp1.ComprobarAlta(nif))
                {
                    //compruebo ha fichado al entrar
                    if (emp1.ComprobarEntrada(nif) == 1)
                    {
                        MessageBox.Show("Este usuario ya ha fichado.");
                    }
                    else
                    {
                        MessageBox.Show("Puede fichar.");
                        //hago la entrada con un INSERT
                    }

                }
            }
            else
            {
               
                MessageBox.Show("El dni {0} no se corresponde con el de ningun empleado.", emp1.Comprobardni(nif));
            }


            //pedir el intervalo de fechas entre las que se quiere tener informacion sobre la permanencia

            //listar todos los empleados que han fichado en esas fechas con una consulta a la bbdd

            //Calcular la duración total de todos los fichajes (tiempo de permanencia –en minutos o un formato 
            //entendible, por ejemplo, en HH: MM -)

            
        }

        private void btnMantenimiento_Click(object sender, EventArgs e)
        {
            ForValidar formularioDeValidacion = new ForValidar();
            formularioDeValidacion.ShowDialog();
            

        }
    }
}
