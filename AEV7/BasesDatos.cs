﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace AEV7
{
    static class BasesDatos
    {


        // atributo para gestionar la conexión
        //static MySqlConnection conexion;
        static string server = "server=127.0.0.1;";
        static string port = "port=3306;";
        static string database = "database=empleados;";
        static string usuario = "uid=root;";
        static string password = "pwd=;";
        static string convert = "Convert Zero Datetime=True;";
        static string connectionstring = server + port + database + usuario + password + convert;

        static MySqlConnection conexion = new MySqlConnection(connectionstring);

        public static MySqlConnection  Conexion { get=>conexion;  }



        // Propiedad para acceder a la conexión


        // Constructor que instancia la conexión, definiendo la cadena de conexión (ConnectionString)



        // Método que se encarga de abrir la conexión
        // Devuelve true/false dependiendo si la conexión se ha abierto con éxito o no
        public static bool AbrirConexion()
        {
            try
            {
                MessageBox.Show("funciona");
                conexion.Open();
                return true;
            }
            catch (MySqlException ex)  // Inicialmente no es necesario utilizar el objeto ex
            {
                MessageBox.Show(" no funciona");
                return false;
            }
        }

        // Método que se encarga de cerrar la conexión (evitar dejar conexiones abiertas)
        // Devuelve true/false dependiendo si la conexión se ha cerrado con éxito
        public static bool CerrarConexion()
        {
            try
            {
                conexion.Close();
                return true;
            }
            catch (MySqlException ex) // Inicialmente no es necesario utilizar el objeto ex
            {
                return false;
            }
        }









    }
}
